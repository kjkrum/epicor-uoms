# Units of Measure in Epicor ERP #
This guide to setting up units of measure (UOMs) in Epicor ERP is based on my experience configuring and administering Epicor 10 and Kinetic for SMBs in the manufacturing sector. This document was condensed from a series of posts on my now-defunct code blog.

## Overview ##

Units of measure are one of the most critical aspects of setting up Epicor. Not only are they foundational to many other parts of the system, they're very easy to set up incorrectly and very complex and risky to try to fix. If you use the wrong inventory UM on a part, you'll probably have to create a new part. If you set up your UOM classes incorrectly, you may have to rebuild your entire system from scratch. The stakes are high, documentation is thin, and there are bugs that can bite you.

First, a quick summary of the main database tables involved in Epicor's UOM system:

* `UOM`: Defines UOM codes, symbols, and descriptions.
* `UOMClass`: Defines groups of UOMs that can be converted to one another.
* `UOMConv`: Identifies a UOM as a member of a class and provides its conversion factor to the base UOM of the class.
* `PartUOM`: Overrides the conversion factor of a UOMConv that is marked part-specific.
* `Part`: Identifies the thing being counted or measured.

These tables have the following relationships:

* `UOMConv` references one `UOMClass` and one `UOM`.
* `Part` references one `UOMClass` and one or more `UOM`s. (Dual-classing is a whole other ball of wax that I have no experience with.)
* `PartUOM` directly references one `Part` and one `UOM`, and indirectly references one `UOMClass` via the `Part`.

This table structure and Epicor's business rules give rise to the following implied relationships:

* The existence of a `Part` referencing a `UOMClass` and `UOM` implies the existence of a `UOMConv` referencing the same `UOMClass` and `UOM`.
* The existence of a `PartUOM` implies the existence of a part-specific `UOMConv` that references the same `UOMClass` and `UOM`.
* The existence of a part-specific `UOMConv` implies the existence of a `PartUOM` for every `Part` referencing the same `UOMClass` and `UOM`.
* The existence of a non-part-specific `UOMConv` implies the non-existence of `PartUOM`s referencing the same `UOMClass` and `UOM`.

Thanks to multiple bugs, none of these implied relationships are reliable. My overall UOM strategy takes this into consideration. I think this strategy would be the right one even without the bugs, but the way it sidesteps the bugs adds weight to the idea that it's right.

Further complicating things, `UOMConv` and `PartUOM` both have "used" flags that are set whenever anything references a conversion. These flags are mainly set by transactions involving parts, but they can also be set by things like adding a part to a bill of materials. When a conversion is marked used, the conversion factor is locked in. Used flags are not directly maintainable, and they are never cleared even if the reference that set them is deleted.

If a conversion is marked used during development and you need to change the conversion factor, the only way to fix it is with a direct SQL update. A SQL update is a very sharp tool that is not recommended or supported by Epicor. If you brute force a conversion factor or used flag, you must be absolutely sure that whatever referenced the conversion has been deleted, or you risk putting your system into an inconsistent state that will blow up or calculate things incorrectly somewhere down the road. Obviously, it's better to avoid getting into this situation in the first place. The strategy I describe here also takes this into consideration.

Don't create your entire UOM system up front. Create it incrementally and keep it flexible. Add UOMs and classes only as needed for the parts you're creating. It's easy to create things, but often nearly impossible to remove them.

## Process ##

Creating records in a certain order helps avoid bugs. The sequence I describe here is not meant to be completed for the entire system in a single pass. Instead, follow this process for each group of parts you create.

### `UOM` ###

Key properties of `UOM` include code, symbol, and number of decimal places. Codes and symbols may appear in EDI documents and other reports, including customer-facing documents like invoices.

Once created, codes cannot be changed. This includes their case. For interoperability, I strongly recommend using ANSI standard two-character uppercase UOM codes whenever a standard exists. If you use any non-standard codes, make them longer than two characters to avoid possible collisions with future standards.

Unlike codes, symbols can be changed later. This is where you might want to use familiar abbreviations like SqFt.

A key thing that can be difficult to explain to users is that a UOM is *not* a description of an item's packaging. Just because something comes in a box does not imply that the UOM is Box. If it's inventoried, sold, used, and scrapped as a unit, it's an Each. The same principle applies to other descriptive UOMs like Bottle, Can, Drum, Bag, or Roll. If you don't track quantities in partial units, they're Eaches. The only time I would consider bending this rule is when there's a strong industry-specific convention for the UOM used for a particular product, or when a vendor who isn't following this rule themselves requires you to order in a specific unit.

You may run into a scenario where you have a raw material that's treated like an Each in most ways, but you need a UOM with decimal places so you can express the fraction of a unit that's used by a nested part. Every time I've encountered a situation like this, there were other factors that justified putting the material in its own custom UOM class.

### `UOMClass` ###

Key properties of `UOMClass` include class ID, class type, and base UOM. Epicor recognizes certain built-in class types including Weight, Length, and several others. Although the classes themselves are not built-in, for simplicity I will refer to classes of built-in types as *built-in classes*. You can only have one class of each built-in type, and each UOM can only be a member of one built-in class.

Certain built-in classes must exist in order to set columns in other tables. For example, a Length class must exist in order to set dimensions on parts. But don't automatically assume that you need classes of every built-in type. Creating UOM classes you don't need causes clutter and increases the chance of making a mistake you can't undo.

Classes of type Other have no special significance to the system. I will refer to classes of this type as *custom classes*. Custom classes are less restrictive than built-in classes. You can have any number of custom classes, and a UOM can be a member of any number of custom classes.

Some use cases for custom classes are more compelling than others. Perhaps the strongest is that you should always create custom classes for groups of parts that need to be converted between units of different types like length and weight. For example, aluminum extrusions may be inventoried by count, used by length, and scrapped by weight. This makes extrusion a prime candidate for its own custom class.

Conversely, never mix UOMs of different types like length and weight within the same built-in class. If you're tempted to do this, it strongly suggests that you should create a custom class. If you mix UOM types and later need to use the out-of-place UOM in its proper class, you won't be able to because of the restriction on UOMs appearing in more than one built-in class. This leads to the creation of redundant UOMs with different and inevitably non-standard codes.

Designing UOM classes is a balancing act between two opposing ideals. Avoid creating classes containing numerous UOMs that only apply to a few parts in the class, but also avoid a confusing proliferation of UOM classes.

### `UOMConv` ###

Key properties of `UOMConv` include the conversion factor to the base UOM of the class, and whether this factor is part-specific. For example, if the base UOM were Pound, the `UOMConv` for Ounce would be set to divide by 16.

If a conversion is not part-specific, the factor in the `UOMConv` is used directly. However, if the conversion *is* part-specific, the `UOMConv` merely supplies the default value for the conversion factor in new `PartUOM`s. A common example of a part-specific conversion would be the number of Eaches in a Box.

I strongly recommend setting a conversion factor of zero on all part-specific conversions. This forces you to supply a valid factor for each new `PartUOM`, which helps you avoid accidentally locking in an incorrect value. If you accept this advice, you might want to enforce it with a Data Directive.

Finally, never create part-specific UOM conversions in built-in classes. For example, consider creating a custom class for the common shipping and receiving UOM hierarchy of Each, Pack, Box, Case, and Pallet. I was on the fence about this until support showed me internal documentation claiming that Epicor does not allow part-specific conversions in built-in classes. When I pointed out that it does, support threatened to "fix" it, and there's no telling how your system might behave if they ever do.

### `PartUOM` ###

These records are not created directly. When a `Part` is created, Epicor automatically creates a `PartUOM` for every part-specific `UOMConv` in the part's `UOMClass`. The default conversion factor provided by the `UOMConv` can then be edited in the UOMs tab of the Part screen.

## Bugs, bugs, bugs! ##

I've encountered three major bugs related to UOMs. As usual, Epicor support is adamant that everything is Working As Intended™, even after providing me with internal documentation that describes intended behavior starkly different from the observable behavior.

### Referential integrity of `UOMConv` ###

Epicor performs referential integrity checks when deleting many types of records, but misses a few for `UOMConv` and `UOM`. It allows you to delete a `UOMConv` while the combination of `UOMClass` and `UOM` is referenced by a `Part`. Once you've deleted a referenced `UOMConv`, it also allows you to delete the referenced `UOM`.

Impact: This bug is relatively fail-fast. Affected parts become unusable. For example, attempting to add one to a sales order results in an immediate crash. Recreating the deleted `UOM`s and `UOMConv`s *seems* to put things back in order, but you never know.

Workaround: Be extremely careful when deleting a `UOMConv`. If you need to delete several, it may be worth writing a Data Directive to prevent mistakes. And there may be other references that Epicor fails to check besides the ones affecting parts. Since we already know we can't trust Epicor here, do everything you can to avoid ever needing to delete a `UOMConv`.

### Orphaned `PartUOM` ###

Epicor allows you to uncheck "part-specific" on a `UOMConv` *after* `PartUOM`s have been created from it, but does not delete the `PartUOM`s.

Impact: An orphaned `PartUOM` remains visible in the UOMs tab on the Part screen, but it's grayed out. In some cases the conversion does not seem to be effective, meaning it's never used in calculations and just causes UI and database clutter. But I've seen evidence that it does remain effective in some circumstances. I haven't tested it thoroughly, but I think one trigger for it remaining effective is "Track Multiple UOMs" being checked on the part. In that case, you're stuck with an incorrect conversion that can't be edited.

Epicor should look for `PartUOM`s when you uncheck "part-specific" on a `UOMConv`. If `PartUOM`s exist but haven't been used, they should be deleted. If they *have* been used, Epicor should prevent you from unchecking "part-specific."

Workaround: If you haven't used the parts, you can delete orphaned `PartUOM`s by deleting and recreating the parts. If you *have* used the parts, it's time to get out the big hammer.

I'm not sure if a BPM can delete `PartUOM`s in a SaaS environment where things are a bit more locked down. But you should at least be able to prevent "part-specific" from being unchecked. If this reveals that your UOM structure is corrupted, at least you'll know.

### Missing `PartUOM` ###

Epicor creates `PartUOM`s for existing part-specific `UOMConv`s when a `Part` is created, but it does *not* create `PartUOM`s for existing parts when a new part-specific `UOMConv` is created. It doesn't even create a `PartUOM` when the conversion is used. It only creates the `PartUOM` when the default factor is edited in the UOMs tab of the Part screen.

Impact: At first glance, Epicor seems to have no problem with missing `PartUOM`s. It simply uses the default factor from the `UOMConv`. The problem is that there's no proper place to mark a non-existent `PartUOM` as used. And instead of creating the missing `PartUOM`, Epicor incorrectly marks the `UOMConv` as used. This is wrong in a couple ways. Since part-specific `UOMConv`s are not used directly in the normal scenario where a `PartUOM` exists, they should never be marked used. Marking a part-specific `UOMConv` as used annoyingly prevents you from changing the default factor that it provides to future `PartUOM`s. But worse, it does not prevent you from subsequently creating the missing `PartUOM` by editing the factor in the Part screen! This would make it appear that historical transactions used the wrong factor.

Workaround: Always create UOMs, classes, and conversions *before* creating the parts that use them. A default factor of zero on part-specific conversions avoids this bug by preventing the conversion from being used until you've manually created a `PartUOM` with a valid factor.

There are forum threads about writing BPMs to create missing `PartUOM`s whenever a new part-specific `UOMConv` is added. This becomes unnecessary (and wouldn't work anyway) if you set the conversion factor on part-specific `UOMConv`s to zero.

## A Robust Solution ##
Attaching the following Data Directive to `UOMConv` prevents all of the aforementioned issues. Ideally, this DD would be one of the first things you set up on a new system, before creating any UOM conversions or parts. If your system has already been corrupted and you can't rebuild it from scratch, this DD will at least prevent you from creating more problems going forward.

The DD consists of a single custom code block. Add references to `Erp.Contracts.BO.Part` and `Erp.Contracts.BO.UOMClass`.

```C#
/*
1. [bug fix] Prevent user from deleting UOMConv referenced by Part.
2. [bug fix] Prevent user from deleting UOMConv if related PartUOM is marked used.
3. [bug fix] Prevent user from unchecking PartSpecific if related PartUOM is marked used.
4. [bug fix] Delete related PartUOMs if deleting UOMConv is allowed.
5. [bug fix] Delete related PartUOMs if unchecking PartSpecific is allowed.
6. [bug fix] Prevent user from checking PartSpecific if UOMConv is marked used.
7. [bug fix] Prevent Epicor from marking part-specific UOMConv used when PartUOM is missing.
8. [best practice] Prevent user from creating a part-specific UOMConv with a non-zero factor.
9. [best practice] Prevent user from creating a part-specific UOMConv in a built-in class.
*/
foreach(var row in ttUOMConv.Where(c => c.Added() || c.Updated()))
{
  /* 6 and 7. Could differentiate error messages, but the result would be the same. */
  if(row.HasBeenUsed && row.PartSpecific)
  {
    throw new Ice.BLException("HasBeenUsed and PartSpecific are mutually exclusive.");
  }
  /* 8 and indirectly 7. */
  if(row.PartSpecific && row.ConvFactor != 0)
  {
    throw new Ice.BLException("PartSpecific requires a ConvFactor of zero.");
  }
}

/* Identify updated rows where PartSpecific changed. */
var psChanged = ttUOMConv.Where(up => up.Updated() && ttUOMConv.Single(un => un.Unchanged() && un.SysRowID == up.SysRowID).PartSpecific != up.PartSpecific);

foreach(var row in ttUOMConv.Where(c => c.Added() && c.PartSpecific).Union(psChanged.Where(o => o.PartSpecific)))
{
  CallService<Erp.Contracts.UOMClassSvcContract>(svc =>
  {
    var ds = svc.GetByID(row.UOMClassID);
    /* 9. */
    if(ds.UOMClass.Single().ClassType != "Other")
    {
      throw new Ice.BLException("PartSpecific only allowed in class type Other.");
    }
  });
}

foreach(var row in ttUOMConv.Where(c => c.Deleted()).Union(psChanged.Where(o => !o.PartSpecific)))
{
  CallService<Erp.Contracts.PartSvcContract>(svc =>
  {
    /* Page through Parts to minimize memory footprint. Requires validation pass and update pass. */
    var pageSize = 10;
    var absolutePage = 0;
    var morePages = false;
    var partUomsExist = false;
    
    do
    {
      var ds = svc.GetRows(
        $"UOMClassID = '{row.UOMClassID}'", /* Part */
        "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1",
        $"UOMCode = '{row.UOMCode}'", /* PartUOM */
        "0=1", "0=1",
        pageSize,
        ++absolutePage,
        out morePages
      );
      /* 1. */
      if(row.Deleted() && ds.Part.Any(o => o.IUM == row.UOMCode || o.PUM == row.UOMCode || o.SalesUM == row.UOMCode))
      {
        throw new Ice.BLException("Cannot delete UOMConv referenced by Part.");
      }
      /* 2 and 3. */
      if(ds.PartUOM.Any(o => o.HasBeenUsed))
      {
        var action = row.Deleted() ? "delete UOMConv" : "uncheck PartSpecific";
        throw new Ice.BLException($"Cannot {action} after related PartUOM HasBeenUsed.");
      }
      partUomsExist |= ds.PartUOM.Any();
    }
    while(morePages);
    
    /* Update pass could race with invalidating changes to PartUOMs. */
    if(partUomsExist)
    {
      absolutePage = 0;
      do
      {
        var ds = svc.GetRows(
          $"UOMClassID = '{row.UOMClassID}'", /* Part */
          "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1", "0=1",
          $"UOMCode = '{row.UOMCode}'", /* PartUOM */
          "0=1", "0=1",
          pageSize,
          ++absolutePage,
          out morePages
        );
        if(ds.PartUOM.Any())
        {
          /* 4 and 5. */
          foreach(var pu in ds.PartUOM)
          {
            pu.RowMod = "D"; 
          }
          svc.Update(ref ds);
        }
      }
      while(morePages);
    }
  });
}

```